﻿CREATE TABLE [Uporabnik] (
    [username] VARCHAR (50) NOT NULL,
    [ime]      VARCHAR (50) NOT NULL,
    [geslo]    VARCHAR (50) NOT NULL,
    [priimek]  NCHAR (50)   NOT NULL,
    [admin]    INT          NOT NULL,
    PRIMARY KEY CLUSTERED ([username] ASC)
);



CREATE TABLE [Pogovor] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [username] VARCHAR (50) NULL,
    [besedilo] NCHAR (50)   NOT NULL,
    [time]     DATETIME     DEFAULT (getdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([username]) REFERENCES [dbo].[Uporabnik] ([username])
);

