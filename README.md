### Naslov projekta ###
ServiceChat

### Avtorji ###
* Nejc Ravnik, 63140219
* Simon Zajc, 63140292

### Kaj je to? ###

Projekt Service je .net implementacija spletne klepetalnice z dodatkom android odjemalnika.

### Kako začeti? ###

* Celoten projekt preneseš na svoj disk.
* Poženeš visual studio verstion control, ki le lociran v root-u projekta.
* V visual studio poženeš: Debug -> Start without debuging

* Za aplikacijo poženeš Android studio.
* V projektu poiščeš datoteko AndroidStudioProject in v njej izbereš datoteko ChatDB kot android studio projekt.
* Če gradle pojamra da potrebno poinštalirat kašne BuildToolse ali SDK, poinštaliraš
* Program nato v emulatorju poženeš z klikom na zeleni "play" gumb

### Zaslonske maske ###
* VSTOP V ADMIN KONZOLO KI JO IMAJO SAMO ADMINI

![zas1.png](https://bitbucket.org/repo/LqXpMa/images/1204441382-zas1.png)

* ADMIN KONZOLA

![zas2.png](https://bitbucket.org/repo/LqXpMa/images/1555298966-zas2.png)

* ANDROID APLIKACIJA LOGIN

![login.png](https://bitbucket.org/repo/LqXpMa/images/2341379017-login.png)

* ANDROID APLIKACIJA CHAT

![chat.png](https://bitbucket.org/repo/LqXpMa/images/2622239775-chat.png)

### Delovanje ###

* Web

Uporabniki se morajo pred uporabo registrirati,
njihovi podatki pa se shranijo v bazo podatkov. Po registraciji se lahko z izbranim uporabniškim imenom in geslom
prijavi v aplikacijo nato pa lahko vidi in hkrati pošilja sporočila vsem drugim prijaveljenim uporabnikom.
Aplikacija omogoča tudi prebiranje sporočil za nazaj, saj so le ta hranjena v bazi podatkov in so vedno na voljo
vsem registriranim uporabnikom. Admin ima na voljo tudi administratorsko konzolo, kjer lahko dodaja pravice drugim članom ter člane odstranjuje.

* Android

Že registriran uporabnik se lahko v isti chat z isto bazo in isto zgodovino vpiše tudi preko android aplikacije. V android aplikaciji lahko prebira celotno zgodovino pogovora in dodaja svoje odgovore.

### Težave ###

* Web

* Android

Pri aplikaciji ni bilo večjih težav, omeniti gre mogoče le to, da sva imela probleme pri pošiljanju sporočila v url naslovu, saj je pri tem potrebno zakodirani posebne znake in presledke.

### Izboljšave ###

Izboljšav pri aplikaciji bi lahko bilo veliko, največ pa bi se dalo narediti na področju funkcionalnosti. Dodali bi lahko VoIP, pošiljanje datotek, slik itd.Prav tako bi se lahko dodale malce bolj napredne knižnjice za dostop do webServica saj so tukaj nekatere metode že ozačene kot deprecated.

### Opis nalog študentov ###

Nejc: Nadgradnja obstoječe web aplikacije, izdelava administratorske konzole, potisk projekta na azure strežnike in QA

Simon: Android odjemalec in QA

### Seznam uporabnikov ###

* ui: admin geslo: Geslo.01