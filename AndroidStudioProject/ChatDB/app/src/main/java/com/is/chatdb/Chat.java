package com.is.chatdb;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Chat extends AppCompatActivity {

    EditText et;
    ArrayList<String> listItems = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    String uName;
    String uPass;
    int lastID;
    String[] params = new String[2];
    ListView lv;
    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent it = getIntent();
        Bundle bundle = it.getExtras();
        uName = bundle.getString("com.is.chatdb.ui");
        uPass = bundle.getString("com.is.chatdb.pass");

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_chat);

        Button bt = (Button) findViewById(R.id.button2);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                params[0] = "messages/" + lastID;
                callREST(params);
            }
        });

        bt = (Button) findViewById(R.id.button3);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //et = (EditText) findViewById(R.id.editText3);
                String vsebina = et.getText().toString();
                if(vsebina != null && !vsebina.equals("")){
                    params[0] = "entermessage/";
                    params[1] = Html.escapeHtml(vsebina).replaceAll(" ", "%20");
                    msg = vsebina;
                }
                callREST(params);
            }
        });

        et = (EditText) findViewById(R.id.editText3);
        et.setHint("Vnesite sporočilo");

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);
        lv = (ListView) findViewById(R.id.lv);
        lv.setAdapter(adapter);

        params[0] = "messages";
        callREST(params);
    }

    private void callREST(String[] par){
        RestCallTask restTask = new RestCallTask();
        restTask.execute(par);
        adapter.notifyDataSetChanged();
    }

    private class RestCallTask extends AsyncTask<String, Void, String> {
        private final String SERVICE_URL = "http://friwebchat.azurewebsites.net/WebService/Service1.svc/%s";
        boolean sendMessage;

        @Override
        protected String doInBackground(String... params){
            sendMessage = params[0].equals("entermessage/");
            String callURL;
            if(!sendMessage) {
                callURL = String.format(SERVICE_URL, params[0]);
            } else {
                callURL = String.format(SERVICE_URL, params[0] + params[1]);
                //msg = params[1];
                Log.d("url: ", callURL);
            }
            //String callURL = "http://friwebchat.azurewebsites.net/WebService/Service1.svc/login/admin/Geslo.01";

            DefaultHttpClient hc = new DefaultHttpClient();
            String result = null;

            try{
                if(!sendMessage) {
                    HttpGet request = new HttpGet(callURL);
                    request.setHeader("Accept", "application/json");
                    request.setHeader("Authorization", uName + ":" + uPass);
                    //Log.d("Kr neki", request.toString());
                    HttpResponse response = hc.execute(request);

                    HttpEntity httpEntity = response.getEntity();
                    result = EntityUtils.toString(httpEntity);
                } else {
                    HttpPost request = new HttpPost(callURL);
                    request.setHeader("Accept", "application/json");
                    request.setHeader("Authorization", uName + ":" + uPass);
                    //Log.d("Kr neki", request.toString());
                    HttpResponse response = hc.execute(request);

                    HttpEntity httpEntity = response.getEntity();
                    result = EntityUtils.toString(httpEntity);
                }
                /*
                request.setHeader("Accept", "application/json");
                request.setHeader("Authorization", uName + ":" + uPass);
                //Log.d("Kr neki", request.toString());
                HttpResponse response = hc.execute(request);

                HttpEntity httpEntity = response.getEntity();
                result = EntityUtils.toString(httpEntity);
                */
            } catch (IOException e){
                e.printStackTrace();;
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if(!sendMessage) {
                try {
                    JSONArray jArray = new JSONArray(result);
                    for (int i = 0; i < jArray.length(); i++){
                        JSONObject jsonObject = jArray.getJSONObject(i);
                        lastID = jsonObject.getInt("Id");
                        listItems.add(jsonObject.getString("Username") + "(" + jsonObject.getString("Time") + "): " + jsonObject.getString("Text"));
                        lv.setSelection(adapter.getCount() - 1);
                    }
                } catch (org.json.JSONException e) {
                    e.printStackTrace();
                }
            } else {
                listItems.add(uName + "(" + new SimpleDateFormat("HH:mm:ss").format(new Date()) + "): " + msg);
                adapter.notifyDataSetChanged();
                lv.setSelection(adapter.getCount() - 1);
                lastID++;
                et.setText("");
            }
        }
    }
}
