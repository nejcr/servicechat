package com.is.chatdb;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

public class LogIn extends AppCompatActivity {

    EditText et;
    Button bt;
    String uName;
    String uPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_log_in);

        bt = (Button) findViewById(R.id.button);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String [] par = new String[2];
                String q;
                et = (EditText) findViewById(R.id.editText);
                q = et.getText().toString();
                par[0] = (q==null) ? "" : q;
                et = (EditText) findViewById(R.id.editText2);
                q = et.getText().toString();
                par[1] = (q==null) ? "" : q;

                callREST(par);
            }
        });

        et = (EditText) findViewById(R.id.editText);
        et.setHint("Uporabnisko ime");
        et = (EditText) findViewById(R.id.editText2);
        et.setHint("Geslo");
    }

    private void callREST(String[] par){
        RestCallTask restTask = new RestCallTask();
        restTask.execute(par);
    }

    private class RestCallTask extends AsyncTask<String, Void, String> {
        private final String SERVICE_URL = "http://friwebchat.azurewebsites.net/WebService/Service1.svc/%s";

        @Override
        protected String doInBackground(String... params){
            uName = params[0];
            uPass = params[1];
            //String callURL = String.format(SERVICE_URL, "login/admin/Geslo.01");
            String callURL = "http://friwebchat.azurewebsites.net/WebService/Service1.svc/login";

            DefaultHttpClient hc = new DefaultHttpClient();
            String result = null;

            try{
                HttpGet request = new HttpGet(callURL);
                request.setHeader("Accept", "application/json");
                request.setHeader("Authorization", uName + ":" + uPass);
                Log.d("Kr neki", request.toString());
                HttpResponse response = hc.execute(request);

                HttpEntity httpEntity = response.getEntity();
                result = EntityUtils.toString(httpEntity);
            } catch (IOException e){
                e.printStackTrace();;
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                //JSONArray jArray = new JSONArray(result);
                JSONObject jObject = new JSONObject(result);
                if(jObject.getBoolean("Status")){
                    Bundle bundle = new Bundle();
                    bundle.putString("com.is.chatdb.ui", uName);
                    bundle.putString("com.is.chatdb.pass", uPass);
                    Intent it = new Intent(LogIn.this, Chat.class);
                    it.putExtras(bundle);
                    startActivity(it);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LogIn.this);
                    builder.setMessage("NAPACNO GESLO IN/ALI VPISNA");
                    builder.setTitle("NAPAKA");
                    builder.setPositiveButton("OK", null);
                    builder.create().show();
                }
            } catch (org.json.JSONException e){
                e.printStackTrace();
            }
        }
    }
}
