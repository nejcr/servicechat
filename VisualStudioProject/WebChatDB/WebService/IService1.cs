﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WebChatDB.WebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [WebGet(UriTemplate = "login",RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Request Login();

        [OperationContract]
        [WebGet(UriTemplate = "messages/{Id}/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<Message> getMessagesFromId(String Id);

        [OperationContract]
        [WebGet(UriTemplate = "messages", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<Message> getMessages();

    

        [OperationContract]
        [WebInvoke(UriTemplate = "entermessage/{message}", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]

        Request AddMessage(string message);

    }


    [DataContract]
    public class Message
    {

        [DataMember]
        public string Id
        {
            get;
            set;
        }



        [DataMember]
        public string Username
        {
            get;
            set;
        }


        [DataMember]
        public string Time
        {
            get;
            set;
        }


        [DataMember]
        public string Text
        {
            get;
            set;
        }

        [DataMember]
        public string MessageStatus
        {
            get;
            set;
        }
        [DataMember]

        public bool Status
        {
            get;
            set;
        }
    }






    [DataContract]

    public class Request
    {

        [DataMember]
        public string MessageStatus
        {
            get;
            set;
        }


        [DataMember]
        public bool Status
        {
            get;
            set;
        }
    }
}
