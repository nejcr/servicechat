﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WebChatDB.DAO;
using WebChatDB.entity;

namespace WebChatDB.WebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1

    {
        UserDaoImp userdatabase= new UserDaoImp();
        ChatDaoImp chatdatabase= new ChatDaoImp();
        

        public Request Login()
        {
            WebOperationContext auth = WebOperationContext.Current;
            string check = auth.IncomingRequest.Headers[HttpRequestHeader.Authorization];
            
            Request req = new Request();

            if (check == null)
            {
                req.MessageStatus = "Prazen request header";
                req.Status = false;
                return req;
            }
            string[] login = check.Split(':');

            if (userdatabase.userExists(login[0]) && userdatabase.getMD5HashfromDB(login[0]).Equals(toMD5(login[1])))
            {
                Console.WriteLine(login[1]);
                req.MessageStatus = "PRIJAVA JE USPELA";
                req.Status = true;
            }
            
            else
            {
                req.MessageStatus = "NAPAKA PRI PRIJAVI";
                req.Status = false;

            }

            return req;
        }

        public List<Message> getMessagesFromId(String Id)
        {
            WebOperationContext auth = WebOperationContext.Current;
            string check = auth.IncomingRequest.Headers[HttpRequestHeader.Authorization];

            // System.Diagnostics.Debug.WriteLine("error: " + login[0] + " " + login[1]);
            //  System.Diagnostics.Debug.WriteLine("error: " + check);
            var Messages = new List<Message>();
            if (check == null)
            {
                Messages.Add(new Message { MessageStatus = "Prazen request header", Status = false });
                return Messages;
            }


            string[] login = check.Split(':');
            if (userdatabase.userExists(login[0]) && userdatabase.getMD5HashfromDB(login[0]).Equals(toMD5(login[1])))
            {
                List<ChatEntity> entities = chatdatabase.getAllChatsFromID(Id);

                foreach (var chat in entities)
                {
                    Messages.Add(new Message { Id = chat.Id, Username = chat.Username, Text = chat.Message, Time = chat.Time, MessageStatus = "Sporočilo prebranao", Status = true });
                }




            }
            else
            {
                Messages.Add(new Message { MessageStatus = "Napaka pri avtentikaciji", Status = false });
            }

            return Messages;
        }

        public List<Message> getMessages()



        {
            

            WebOperationContext auth = WebOperationContext.Current;
            string check = auth.IncomingRequest.Headers[HttpRequestHeader.Authorization];
            
            // System.Diagnostics.Debug.WriteLine("error: " + login[0] + " " + login[1]);
            //  System.Diagnostics.Debug.WriteLine("error: " + check);
            var Messages = new List<Message>();
            if (check == null)
            {
                Messages.Add(new Message { MessageStatus = "Prazen request header", Status = false });
                return Messages;
            }


            string[] login = check.Split(':');
            if (userdatabase.userExists(login[0]) && userdatabase.getMD5HashfromDB(login[0]).Equals(toMD5(login[1])))
            {
                List<ChatEntity> entities = chatdatabase.getAllChats();
                
                foreach (var chat in entities)
                {
                    Messages.Add(new Message {Id = chat.Id, Username = chat.Username,Text = chat.Message, Time = chat.Time, MessageStatus = "Sporočilo prebranao", Status = false});
                }

                


            }
            else
            {
                Messages.Add(new Message {MessageStatus = "Napaka pri avtentikaciji",Status = false });
            }

            return Messages;
        }

       

        public Request AddMessage(string message)
        {
            WebOperationContext auth = WebOperationContext.Current;
            string check = auth.IncomingRequest.Headers[HttpRequestHeader.Authorization];
            Request req = new Request();
            

            if (check == null)
            {
                req.MessageStatus = "Prazen request header";
                req.Status = false;
            }
            string[] login = check.Split(':');

            if (userdatabase.userExists(login[0]) && userdatabase.getMD5HashfromDB(login[0]).Equals(toMD5(login[1])))
            {
                ChatEntity chat = new ChatEntity(login[0], message, DateTime.Now.ToString());
                chatdatabase.insertChat(chat);

                req.MessageStatus = "SPOROČILO POSLANO";
                req.Status = true;


            }

            else
            {
                req.MessageStatus = "Napaka pri avtentikaciji";
                req.Status = false;
            }

            return req;

        }

















        private String toMD5(String password)
        {

            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(password);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();

        }
    }


    
}
