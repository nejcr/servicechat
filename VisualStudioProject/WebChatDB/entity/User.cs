﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebChatDB.entity
{
    public class User
    {

        private String username;
        private String ime;
        private String priimek;
        private String geslo;
        private bool isAdmin;

        public User(string username, string ime, string priimek, string geslo, bool isAdmin)
        {
            this.username = username;
            this.ime = ime;
            this.priimek = priimek;
            this.geslo = geslo;
            this.isAdmin = isAdmin;
        }


        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string Ime
        {
            get { return ime; }
            set { ime = value; }
        }

        public string Priimek
        {
            get { return priimek; }
            set { priimek = value; }
        }

        public string Geslo
        {
            get { return geslo; }
            set { geslo = value; }
        }

        public bool IsAdmin
        {
            get { return isAdmin; }
            set { isAdmin = value; }
        }
    }
}