﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace WebChatDB.entity
{
    public class ChatEntity
    {
        private String id;
        private String username;
        private String message;
        private String time;


        public ChatEntity(string username, string message, string time)
        {
            this.username = username;
            this.message = message;
            this.time = time;
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public string Time
        {
            get { return time; }
            set { time = value; }
        }
    }
}