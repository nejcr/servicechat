﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Chat.aspx.cs" Inherits="WebChatDB.Chat" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">	
    <link href="css/chatstyle.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body>
    <form id="form1" runat="server">
<h1>FRI CHATROOM</h1>
<div class="mainContainer">
	
	<div class =chatContainer>
	
	    <div class="chatHeader">Trenutno ste prijavljeni kot:
            <asp:Label ID="labelCurentUser" runat="server" Text="Nejc Ravnik" CssClass="labelCUser"></asp:Label>
	     
	        
	        <asp:LinkButton ID="adminConsoleLinkButton" runat="server"  OnClick="LinkButton1_Click" ForeColor="#CCCCCC">Admin Console</asp:LinkButton>
	     
	        
	    </div>
		
		    
		
		    <asp:ListBox ID="lbAllMessages" runat="server" CssClass="messages" Height="633px" Width="678px">
               
                <asp:ListItem class="listitem">RokCUND:HEJ </asp:ListItem>
                
                
            </asp:ListBox>
			
		
	
		    <div class ="writeTextContainer">
			
            <asp:TextBox ID="tbMessage" runat="server" CssClass="textbox"></asp:TextBox>
			<asp:Button ID="btnSend" runat="server" Text="Pošlji" CssClass="btnSend" OnClick="btnSend_Click" />

		    </div>






	</div>
	
	<div class="userContainer">
		<div class="userHeader">PRIJAVLJENI UPORABNIKI</div>
		<asp:ListBox ID="lbLoggedOnUsers" CssClass="loggedOnUsers" runat="server">
		    <asp:ListItem class="listofUsers">Nejc Ravnik </asp:ListItem>
          

		</asp:ListBox>

		<div class="rightButtons">
			
		
		
    

		</div>







	</div>


	<div class="loggoutButton">
	    <asp:Button ID="btnRefresh" runat="server" CssClass="button" Text="Osveži" OnClick="btnRefresh_Click" />
		<asp:Button ID="btnLogout" runat="server" CssClass="button" Text="Odjava" OnClick="btnLogout_Click" />
	</div>





<footer>Copyright SimonZajc, Nejc Ravnik
    
    </footer>

</div>

    </form>

</body>
</html>