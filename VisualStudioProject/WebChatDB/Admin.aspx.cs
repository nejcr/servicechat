﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebChatDB.DAO;

namespace WebChatDB
{
    public partial class Admin : System.Web.UI.Page
    {
        UserDaoImp userdata = new UserDaoImp();

        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (Session["Id"] == null)
            {
               
                if( (List<String>) Application["users"]!=null)
                {
                    ((List<String>)Application["users"]).Remove((String)Session["Id"]);
                }
                    
              Response.Redirect("Login.aspx");
                

            }
            else
            {
                String cuser = (String) Session["Id"];
                if (!userdata.checkAdmin(cuser))
                {
                    ((List<String>)Application["users"]).Remove((String)Session["Id"]);
                    Response.Redirect("Chat.aspx");
                }
            }
            
        }

        
        
        protected void adminLogoutBtn_Click(object sender, EventArgs e)
        {
            ((List<String>)Application["users"]).Remove((String)Session["Id"]);
            Session["Id"] = "";
            Response.Redirect("Login.aspx");
        }

        protected void adminBackToChatBtn_Click(object sender, EventArgs e)
        {
            ((List<String>)Application["users"]).Remove((String)Session["Id"]);
            Response.Redirect("Chat.aspx");
        }
    }
}