﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebChatDB.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="login-page">
  <div class="complete-form">
       <div class="login-form">
        <asp:TextBox ID="loginUserName" runat="server" placeholder="Uporabniško ime" CssClass="input"></asp:TextBox>
        <asp:TextBox ID="loginPassword" runat="server" TextMode="Password"  placeholder="Geslo"  CssClass="input"></asp:TextBox>
        <asp:Button ID="loginBtn" runat="server"  CssClass="button" Text="Prijavi se" OnClick="loginBtn_Click" />
        
        
        
     <p class="message1"><a href="#">Registriraj se</a></p> 
           
    </div>

    <div class="register-form">
        <asp:TextBox ID="registerName" runat="server" placeholder="Ime" CssClass="input"></asp:TextBox>
        <asp:TextBox ID="registerLastName" runat="server" placeholder="Priimek" CssClass="input"></asp:TextBox>
        <asp:TextBox ID="registerUserName" runat="server" placeholder="Uporabniško ime" CssClass="input"></asp:TextBox>
        <asp:TextBox ID="registerPassword" runat="server" placeholder="Geslo" CssClass="input" TextMode="Password"></asp:TextBox>
        <asp:TextBox ID="registerRetypePassword" runat="server" placeholder="Ponovite geslo" CssClass="input" TextMode="Password"></asp:TextBox>
        <asp:Button ID="registerButton" runat="server" Text="Registriraj se" CssClass="button" OnClick="registerButtonOnClick"  />
        
       
    <p class="message2"> <a href="#">Prijavi se</a></p> 
    </div>
      <asp:Label ID="ErrorLabel" runat="server" CssClass="message1" ForeColor="#4CAF50"></asp:Label>
   
  </div>
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  
        
           
        <script src="js/toogle.js"></script>

        
    </form>
  

    

</body>
</html>
