﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using WebChatDB.DAO;
using WebChatDB.entity;

namespace WebChatDB
{
    public partial class Login : System.Web.UI.Page
    {

        Regex regx;
        UserDaoImp database = new UserDaoImp();

        protected void Page_Load( object sender, EventArgs e ) {
            regx = new Regex( @"^(?=.{8,})(?=(.*\d.*\d))(?=(.*[?!.*:]+))((.*?[A-Z]){2,}.*$)" );

            if (IsPostBack)
            {
                ErrorLabel.Text = "";
            }
            
        }



        protected void registerButtonOnClick( object sender, EventArgs e ) {

            // if any of the input fields are empty, return
            if ( registerName.Text.Length == 0 || registerLastName.Text.Length==0 || registerUserName.Text.Length == 0 || registerPassword.Text.Length == 0 || registerRetypePassword.Text.Length == 0 ) {
                ClientScript.RegisterStartupScript(this.GetType(), "YourMessage", "alert('Vsa polja morajo biti izpolnjena');", true);
                return;
            }

            if ( !regx.IsMatch(registerPassword.Text) ) {
                ClientScript.RegisterStartupScript(this.GetType(), "YourMessage", "alert('Geslo mora vsebovati: 8 znakov, 2 veliki začetnici, 1 posebenznak (.!?*:) and 2 številki !!');", true);
                return;
            }

            if ( !registerPassword.Text.Equals(registerRetypePassword.Text) ) {
                ClientScript.RegisterStartupScript(this.GetType(), "YourMessage", "alert('Gesli se ne ujemata');", true);
                return;
            }

            


            if (database.userExists(registerUserName.Text))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "YourMessage", "alert('Uporabniško ime je zasedeno');", true);
                return;

            }
            else
            {
                User newUser = new User(registerUserName.Text,registerName.Text,registerLastName.Text,toMD5(registerPassword.Text), false);
                database.insertUser(newUser);
                ErrorLabel.Text = "Registracija uspešna";
                return;
            }






            

        }

        protected void loginBtn_Click(object sender, EventArgs e) {

            if ( loginUserName.Text.Length == 0 || loginPassword.Text.Length == 0 ) {
                ClientScript.RegisterStartupScript(this.GetType(), "YourMessage", "alert('ONE OR BOTH OF THE FIELDS WERE EMPTY!!');", true);
                return;
            }

            if (database.userExists(loginUserName.Text))
            {
                String pass = database.getMD5HashfromDB(loginUserName.Text);
                bool passequals = pass.Equals(toMD5(loginPassword.Text));
                if (!passequals)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "YourMessage", "alert('USERNAME OR PASSWORD NOT CORRECT');", true);
                    return;
                }
            }
            if (!database.userExists(loginUserName.Text))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "YourMessage", "alert('USERNAME OR PASSWORD NOT CORRECT');", true);
                return;
            }
            else
            {
                Session["Id"] = loginUserName.Text;
                Response.Redirect("Chat.aspx");
            }

            



        }

        private String toMD5( String password ) {

            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(password);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();

        }

      

    }
}