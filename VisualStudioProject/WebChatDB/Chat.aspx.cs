﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebChatDB.DAO;
using WebChatDB.entity;

namespace WebChatDB
{
    public partial class Chat : System.Web.UI.Page
    {

        ChatDaoImp database= new ChatDaoImp();
        UserDaoImp databaseUser= new UserDaoImp();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {

                if (Session["Id"] == null)
                {
                   Response.Redirect("Login.aspx");

                }
                if (Application["users"] == null)
                {
                    Application["users"] = new List<string>();
                    ((List<String>) Application["users"]).Add((String) Session["Id"]);
                    labelCurentUser.Text = (String) Session["Id"];
                    refreshDataOnPage();
                }

                else
                {
                    if (!checkSession((String) Session["Id"]))
                    {
                        ((List<String>)Application["users"]).Add((String)Session["Id"]);
                    }
                    
                    refreshDataOnPage();
                }
                
                
            }

            if (databaseUser.checkAdmin((String) Session["Id"]))
            {
                adminConsoleLinkButton.Visible = true;
            }

            if (!databaseUser.checkAdmin((String)Session["Id"]))
            {
                adminConsoleLinkButton.Visible = false;
            }
            if (!IsPostBack)
            {

                refreshDataOnPage();
            }
            labelCurentUser.Text = (String)Session["Id"];
            

        }

        public void refreshDataOnPage()
        {
            lbAllMessages.Items.Clear();
            lbLoggedOnUsers.Items.Clear();
            foreach (String user in (List<string>) Application["users"])
            {
                ListItem curentUserItem = new ListItem(user);
                curentUserItem.Attributes.Add("class", "listofUsers");
                lbLoggedOnUsers.Items.Add(curentUserItem);
            }


            foreach (string message in database.getAllChatsForBrowser())
            {
                ListItem currentMessageItem = new ListItem(message);
                currentMessageItem.Attributes.Add("class", "listitem");
                lbAllMessages.Items.Add(currentMessageItem);
            }

            
        }




        protected void btnSend_Click(object sender, EventArgs e)
        {
            ChatEntity message = new ChatEntity((String) Session["Id"], tbMessage.Text,  DateTime.Now.ToShortTimeString());
            database.insertChat(message);
            refreshDataOnPage();
            tbMessage.Text = "";
        }




        protected void btnLogout_Click(object sender, EventArgs e)
        {

            ((List<String>)Application["users"]).Remove((String)Session["Id"]);
            Session["Id"] = "";
            Response.Redirect("Login.aspx");

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            refreshDataOnPage();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Admin.aspx");
        }

        private bool checkSession(String ses )
        {
            foreach (String user in (List<string>) Application["users"])
            {
                if (ses.Equals(user))
                {
                     return true;
                }

            }
            return false;
        }
    }
}