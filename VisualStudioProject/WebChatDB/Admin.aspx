﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="WebChatDB.Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Console</title>
    <link href="css/admin.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h1>WebChatDB Admin Console</h1>
    </div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" CssClass="table1" AllowPaging="True" AllowSorting="True">
            <Columns>
                <asp:BoundField DataField="username" HeaderText="username" SortExpression="username" />
                <asp:BoundField DataField="Število sporočil" HeaderText="Število sporočil" ReadOnly="True" SortExpression="Število sporočil" />
            </Columns>
            <EditRowStyle Font-Underline="False" />
        </asp:GridView>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="username" DataSourceID="SqlDataSource2" AllowSorting="True" CssClass="table2" AllowPaging="True" EnableTheming="True" Font-Underline="False">
            <Columns >
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="username" HeaderText="username" ReadOnly="True" SortExpression="username"/>
                <asp:BoundField DataField="ime" HeaderText="ime" SortExpression="ime" />
                <asp:BoundField DataField="priimek" HeaderText="priimek" SortExpression="priimek" />
                <asp:BoundField DataField="geslo" HeaderText="geslo" SortExpression="geslo" />
                <asp:CheckBoxField DataField="admin" HeaderText="admin" SortExpression="admin" />
            </Columns>
            <HeaderStyle Font-Strikeout="False" Font-Underline="False" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT username, COUNT(Id) AS &quot;Število sporočil&quot; from pogovor GROUP BY username;"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [Uporabnik] WHERE [username] = @username" InsertCommand="INSERT INTO [Uporabnik] ([username], [ime], [priimek], [geslo], [admin]) VALUES (@username, @ime, @priimek, @geslo, @admin)" SelectCommand="SELECT * FROM [Uporabnik]" UpdateCommand="UPDATE [Uporabnik] SET [ime] = @ime, [priimek] = @priimek, [geslo] = @geslo, [admin] = @admin WHERE [username] = @username">
            <DeleteParameters>
                <asp:Parameter Name="username" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="username" Type="String" />
                <asp:Parameter Name="ime" Type="String" />
                <asp:Parameter Name="priimek" Type="String" />
                <asp:Parameter Name="geslo" Type="String" />
                <asp:Parameter Name="admin" Type="Boolean" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="ime" Type="String" />
                <asp:Parameter Name="priimek" Type="String" />
                <asp:Parameter Name="geslo" Type="String" />
                <asp:Parameter Name="admin" Type="Boolean" />
                <asp:Parameter Name="username" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:Button ID="adminBackToChatBtn" runat="server" Text="Nazaj na klepet"  CssClass="button_admin" OnClick="adminBackToChatBtn_Click"/>
        <asp:Button ID="adminLogoutBtn" runat="server" Text="Odjava"  CssClass="button_admin2" OnClick="adminLogoutBtn_Click"/>
        
        
    </form>
</body>
</html>
