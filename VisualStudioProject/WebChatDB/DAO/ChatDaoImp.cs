﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Management;
using WebChatDB.entity;

namespace WebChatDB.DAO
{
    class ChatDaoImp : ChatDao
    {


        public SqlConnection getConnection()
        {
            try
            {
                SqlConnection conn =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                return conn;
            }
            catch (Exception ex)
            {
                throw new SqlExecutionException();
            }

        }

        public Boolean insertChat(ChatEntity chat)
        {
            SqlConnection conn = null;
            try
            {
                conn = getConnection();
                SqlCommand command = new SqlCommand("Insert into Pogovor(username, besedilo,time) Values(@uporabnik, @message,@time)", conn);
                command.Parameters.AddWithValue("@uporabnik", chat.Username);
                command.Parameters.AddWithValue("@message", chat.Message);
                command.Parameters.AddWithValue("@time", chat.Time);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw new SqlExecutionException();
            }

        }

        public List<String> getAllChatsForBrowser()

        {
            SqlConnection conn = null;
            try
            {
                List<String> returnList = new List<String>();
                conn = getConnection();
                conn.Open();
                SqlCommand command = new SqlCommand("select username,besedilo,time from Pogovor ORDER BY ID", conn);
                SqlDataReader columns = command.ExecuteReader();
                while (columns.Read())
                {
                    String returnString = columns["username"] +" ob "+ " : " + columns["time"] + " "+columns["besedilo"] ;
                    returnList.Add(returnString);
                }
                columns.Close();
                conn.Close();
                return returnList;

            }
            catch (Exception ex)
            {
                throw new SqlExecutionException();
            }
      }

        public List<ChatEntity> getAllChats()
        {
            SqlConnection conn = null;
            try
            {
                List<ChatEntity> returnList = new List<ChatEntity>();
                conn = getConnection();
                conn.Open();
                SqlCommand command = new SqlCommand("select Id,username,besedilo,time from Pogovor ORDER BY ID", conn);
                SqlDataReader columns = command.ExecuteReader();
                while (columns.Read())
                {
                    ChatEntity returnString = new ChatEntity( (String)columns["username"],  (String)columns["besedilo"],columns["time"].ToString());
                    returnString.Id = columns["Id"].ToString();
                    
                    returnList.Add(returnString);
                }
                columns.Close();
                conn.Close();
                return returnList;

            }
            catch (Exception ex)
            {
                throw new SqlExecutionException();
            }
        }

        public List<ChatEntity> getAllChatsFromID(String Id)
        {
            SqlConnection conn = null;
            try
            {
                List<ChatEntity> returnList = new List<ChatEntity>();
                conn = getConnection();
                conn.Open();
                SqlCommand command = new SqlCommand("select Id,username,besedilo,time from Pogovor where Id > "+Id+" ORDER BY ID", conn);
                SqlDataReader columns = command.ExecuteReader();
                while (columns.Read())
                {
                    ChatEntity returnString = new ChatEntity((String)columns["username"], (String)columns["besedilo"], columns["time"].ToString());
                    returnString.Id = columns["Id"].ToString();

                    returnList.Add(returnString);
                }
                columns.Close();
                conn.Close();
                return returnList;

            }
            catch (Exception ex)
            {
                throw new SqlExecutionException();
            }
        }
    }




}
