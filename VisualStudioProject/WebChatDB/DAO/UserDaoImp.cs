﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Management;
using System.Web.UI.WebControls.WebParts;
using WebChatDB.DAO;
using WebChatDB.entity;

namespace WebChatDB.DAO
{
    public class UserDaoImp : UserDao
    {

       

      public SqlConnection getConnection()
        {
            try
            {
                SqlConnection conn =new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                return conn;
            }
            catch (Exception ex)
            {
                throw new SqlExecutionException();
            }
            
        }


    public string getMD5HashfromDB(string username)
    {
        SqlConnection conn = null;

            try
            {
                conn = getConnection();
                
                SqlCommand command = new SqlCommand("SELECT geslo FROM Uporabnik WHERE username=" + "'" + username + "'", conn);
                conn.Open();
                String pass = (String)command.ExecuteScalar();
                conn.Close();
                return pass;

            }

            catch (Exception ex)
            {
                throw new SqlExecutionException();
            }



        
    }

        public bool userExists(string username)
        {
            SqlConnection conn = null;

            try
            {
                conn = getConnection();
                
                SqlCommand command = new SqlCommand("select COUNT(*) from Uporabnik where username=" + "'" + username + "'", conn);
                conn.Open();
                String check = command.ExecuteScalar().ToString();
                conn.Close();
                if (Convert.ToUInt32(check) != 1)
                {
                    return false;
                }
                return true;


            }

            catch (Exception ex)
            {
                throw new SqlExecutionException();
            }

        }


        public bool checkAdmin(string username)
        {
            SqlConnection conn = null;

            try
            {
                conn = getConnection();

                SqlCommand command = new SqlCommand("select admin from Uporabnik where username=" + "'" + username + "'", conn);
                conn.Open();
                bool check = (bool) command.ExecuteScalar();
                
                conn.Close();
                if (check)
                {
                    return true;
                    
                }
                else
                {
                    return false;
                }
               
                


            }

            catch (Exception ex)
            {
                throw new SqlExecutionException();
            }



        }
        public bool insertUser(User user)
        {
            SqlConnection conn = null;

            try
            {
                conn = getConnection();
                SqlCommand command = new SqlCommand("Insert into Uporabnik(username,ime,priimek,geslo,admin) values (@uporabnik,@name, @lastname,@pass, @admin)", conn);
                command.Parameters.AddWithValue("@uporabnik", user.Username);
                command.Parameters.AddWithValue("@name", user.Ime);
                command.Parameters.AddWithValue("@lastname", user.Priimek);
                command.Parameters.AddWithValue("@pass", user.Geslo);
                command.Parameters.AddWithValue("@admin", user.IsAdmin);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
                return true;

            }

            catch (Exception ex)
            {
                throw new SqlExecutionException();
            }


            
        }
    }
}