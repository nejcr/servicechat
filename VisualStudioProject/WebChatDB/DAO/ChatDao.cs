﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebChatDB.entity;

namespace WebChatDB.DAO
{
    interface ChatDao
    {

        SqlConnection getConnection();

        Boolean insertChat(ChatEntity chat);

        List<String> getAllChatsForBrowser();

        List<ChatEntity> getAllChats();

        List<ChatEntity> getAllChatsFromID(String id);


    }
}
