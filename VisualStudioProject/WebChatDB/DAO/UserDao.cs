﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebChatDB.entity;

namespace WebChatDB
{
    public interface UserDao
    {
        SqlConnection getConnection();

        String getMD5HashfromDB(String username);

        Boolean userExists(String username);

        Boolean insertUser(User user);

        Boolean checkAdmin(String username);
    }
}
